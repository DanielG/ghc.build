#!/bin/bash
set -x
NPROC=${NPROC:-$(nproc)}
f=$(mktemp --tmpdir 'XXXXXXXX.Makefile')

cleanup_do () {
        rm -f "$f"
}

trap 'cleanup_do; trap - INT; kill -INT $$' INT
trap 'cleanup_do; trap - TERM; kill -TERM $$' TERM
trap 'cleanup_do; trap - EXIT' EXIT

cat > "$f" <<EOF
go:
	+@exec $(printf '%q ' "$@")
.PHONY: go
EOF

echo ===
cat "$f"
echo ===

make -j$NPROC -f "$f" go
