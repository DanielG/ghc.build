#!/bin/sh

REPO=$PWD/.git

TOPDIR="$(dirname "$0")"
cd $TOPDIR
TOPDIR=$PWD

commits=$(cat commits)

alldone=true
for commits in commits-*; do
        id="${1#commits-*}"
        builddir=builddir-"$id"

        for commit in $(cat $commits); do
                msg=$(git --git-dir="$REPO" log -n1 "$commit" \
                          --pretty=oneline \
                          --abbrev-commit \
                          --color=always \
                              | cat | head -c 75)

                status=$(cat status/$commit 2>/dev/null || echo -)

                if [ x"$status" = x"ok" ]; then
                        printf '%.6s\t%s\n' "ok" "$msg"
                else
                        printf '%.6s\t%s\n' "$status" "$msg"
                fi
        done
done
