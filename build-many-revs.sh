#!/bin/sh

set -ue

our_branch=$1; shift
base_branch=$1; shift # origin/master or whatever
origin=$(git remote get-url origin)

export TOPDIR="$(dirname "$0")"
export REPO="$(git rev-parse --git-common-dir)"
NPROC=${NPROC:-$(nproc)}
NBUILDS=${NBUILDS:-4}
NSUBPROC=${NSUBPROC:-$((NPROC / NBUILDS))}

echo NPROC=$NPROC
echo NBUILDS=$NBUILDS
echo NSUBPROC=$NSUBPROC

cd $TOPDIR
TOPDIR=$PWD

mkdir -p status

base_commit=$(git --git-dir="$REPO" rev-parse "$base_branch")
our_commit=$(git --git-dir="$REPO" rev-parse "$our_branch")

merge_base=$(git --git-dir="$REPO" merge-base "$base_commit" "$our_commit")

git --git-dir="$REPO" rev-list "$our_commit" ^"$merge_base" | tac > commits

commits=$(cat commits)

if [ -z "$commits" ]; then
        echo "No commits!" >&2h
        exit 1
fi

rm commits-* || true
split -n "l/$NBUILDS" commits commits-

for f in commits-*; do
        builddir="builddir-${f#commits-*}"
        if [ ! -f "$builddir"/.git/ghc-ok ]; then (
                #git clone --share "$REPO" "$builddir"

                mkdir -p "$builddir"
                git -C "$builddir" init
                git -C "$builddir" remote remove origin || true
                git -C "$builddir" remote add origin "$REPO"

                echo "$REPO/objects" > "$builddir"/.git/objects/info/alternates
                head -n1 < "$f" > "$builddir"/.git/HEAD

                git -C "$builddir" fetch origin
                touch "$builddir"/.git/ghc-ok
        ); else
                git -C "$builddir" fetch origin
        fi

        # branch=$(git -C "$builddir" rev-parse --abbrev-ref HEAD)
        # git -C "$builddir" remote set-url origin "$origin"
        # git -C "$builddir" config branch.$branch.remote origin
        # git -C "$builddir" submodule sync
done

cat commits-* | xargs -n1 sh -c 'rm -f status/"$1"' rm-status

printf '%s\n' commits-* \
        | xargs -P$NBUILDS -n1 -- env NPROC=$NSUBPROC ./build-rev.sh
rv=$?

exit $rv
