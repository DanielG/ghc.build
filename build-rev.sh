#!/bin/bash
set -uex

id="${1#commits-*}"
commits=$(cat "$1")
builddir=builddir-"$id"

die () {
        echo failed ${1:-} > "$TOPDIR"/status/$commit
}

# put aside stdout/stderr as fd:8/9 for later
exec 8>&1
exec 9>&2

mkdir -p "$builddir"
cd "$builddir"

tee_pid=
first=true
for commit in $commits; do
        if [ -n "${tee_pid}" ]; then
                # This closes all refereces to tee's pipe, making it exit
                # due to EOF. Also wait for it to finish so as to not leave
                # zombies behind.
                exec >&-
                exec 2>&-
                wait ${tee_pid}
        fi

        rm -f "$TOPDIR"/status/$commit

        # redirect stout/stderr to tee which will write to both the log
        # file and the terminal
        exec > >(exec tee "$TOPDIR/status/$commit.log" >&8 2>&9) 2>&1
        tee_pid=$!

        printf '\n\n\n\n\n%s\n' "======= BEGIN COMMIT $commit ===" | tee log

        pwd

        git reset --hard HEAD
        git checkout "$commit"
        git clean -e got_stage1 -f

        if [ ! -e got_stage1 ]; then
                printf '%s\n' \
                       'BuildFlavour = quick' \
                       'include mk/flavours/$(BuildFlavour).mk' \
                       'BUILD_PROF_LIBS = YES' \
                       'STRIP_CMD = :' \
                       > mk/build.mk
        else
                printf '%s\n' \
                       'BuildFlavour = quick' \
                       'include mk/flavours/$(BuildFlavour).mk' \
                       'BUILD_PROF_LIBS = YES' \
                       'STRIP_CMD = :' \
                       'stage=2' \
                       > mk/build.mk
        fi
        if $first && [ ! -e got_stage1 ]; then
                # TODO: Doesn't handle submodule changes in my commits
                #flock  -w 10 "$REPO" git --git-dir="$REPO" submodule update --init
                git submodule update --init
                { ./boot && ./configure GHC=$(which ghc-8.8.3) ; } \
                        || { die "conf"; continue; }
                first=false
        fi
        make -j$NPROC || { die "comp"; continue; }
        touch got_stage1

        ./inplace/bin/ghc-stage2 -O0 \
                -fforce-recomp -prof -debug -rtsopts \
                -package deepseq \
                -o DataTypeMemoryProf \
                ~/dev/hs/testing/DataTypeMemoryProf.hs \
                || { die "test-comp"; continue; }
        ./DataTypeMemoryProf +RTS -hr -s || { die "test-run"; continue; }

        ./DataTypeMemoryProf +RTS -hb -s || { die "test-rhb"; continue; }

        if [ -e rts/TraverseHeapTest.c ] ||
           [ -e testsuite/tests/profiling/should_run/TraverseHeapTest.c ]
        then
                make test TEST=TraverseHeapTest \
                        || { die "test2-run"; continue; }
        fi

        if [ -e testsuite/tests/profiling/should_run/RootProfileTest.hs ]; then
                make test TEST=RootProfileTest \
                        || { die "test3-run"; continue; }
        fi

        echo ok > "$TOPDIR/status/$commit"
done
